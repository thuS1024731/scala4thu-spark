import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac026 on 2017/5/1.
  */
object StrLengthsApp extends App {
val conf=new SparkConf().setAppName("HelloSpark").setMaster("local[*]")
  val sc=new SparkContext(conf)
 val nums: RDD[String]=sc.textFile("nums.txt")
  val strLength: RDD[Int] = nums.map(_.length)
  println(strLength.collect().toList)
  strLength.foreach(len=>println(len))
}
